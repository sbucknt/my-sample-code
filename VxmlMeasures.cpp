/* VXMLI Counters and gauges */

#include <MasAdapt.h>
#include <VxmlMeasures.h>
#include <assert.h>

#define NOENTRY L"NOENTRY"


// translation table for Vxi events and counters (used to populate the VXIMAP)
// This table must be in the same order as VxmlCounterString
VXIchar* VxmlVxiTransString[] = 
{
// Table 15 -   General Counters

/* VXML_NBR_CALLS */ NOENTRY,
/* VXML_NBR_EVTNORESOURCE */  L"error.noresource",
/* VXML_NBR_EVTERRNOAUTH */ L"error.noauthorization",
/* VXML_NBR_EVTERROROBJECT */ L"error.object",
/* VXML_NBR_EVTERRORTRANSFER */ L"error.transfer",
/* VXML_NBR_UNSUPPORTENCODING */ NOENTRY,
/* VXML_NBR_DIALOGS */ NOENTRY,

/*  Table 16 -  Transfer Counters */
/* VXML_NBR_TRANSFERS */ NOENTRY,
/* VXML_NBR_BLINDTRANSFERS */ NOENTRY,
/* VXML_NBR_BRIDGEDTRANSFERS */ NOENTRY,
/* VXML_NBR_CONSULTTRANSFERS */ NOENTRY,
/* VXML_NBR_TRANSFERFAILS */ NOENTRY,
/* VXML_NBR_XFERFAILCALLEEBUSY */ NOENTRY,
/* VXML_NBR_XFERFAILNETBUSY */ NOENTRY,
/* VXML_NBR_XFERFAILNOANSWER */ NOENTRY,
/* VXML_NBR_EVTCONNNOAUTH */ L"error.connection.noauthorization",
/* VXML_NBR_EVTCONNBADDEST */ L"error.connection.baddestination",
/* VXML_NBR_EVTCONNNOROUTE */ L"error.connection.noroute",
/* VXML_NBR_EVTCONNNORSRC */ L"error.connection.noresource",
/* VXML_NBR_EVTUNSUPPORTBLIND */ L"error.unsupported.transfer.blind",
/* VXML_NBR_EVTUNSUPPORTBRIDGE */ L"error.unsupported.transfer.bridge",
/* VXML_NBR_EVTUNSUPPORTURI */ L"error.unsupported.uri",

/*  Table 17 -  Fetching/Parsing Counters */

/* VXML_NBR_FETCH_ATTEMPTS */ NOENTRY,
/* VXML_NBR_EVTBADFETCHES */ L"error.badfetch",
/* VXML_NBR_EVTBADFETCH_URI */ L"error.badfetch.baduri",
/* VXML_NBR_EVTBADFETCH_APPURI */ L"error.badfetch.applicationuri",
/* VXML_NBR_EVTBADFETCH_DIALOG */ L"error.badfetch.baddialog",
/* VXML_NBR_EVTBADFETCHHTTPERR */ NOENTRY,
/* VXML_NBR_DISKCACHEHITS */ NOENTRY,
/* VXML_NBR_DISKCACHEMISS */ NOENTRY,
/* VXML_NBR_MEMORYCACHEHITS */ NOENTRY,
/* VXML_NBR_MEMORYCACHEMISS */ NOENTRY,
/* VXML_NBR_EVTSEMATICERRORS */ L"error.semantic",
/* VXML_NBR_EVTSEMERR_ECMA */ L"error.semantic.ecmascript",
/* VXML_NBR_EVTSEMERR_BADTHROW */ L"error.semantic.no_event_in_throw",
/* VXML_NBR_EVTSEMERR_NOGRAM */ L"error.semantic.nogrammars",
/* VXML_NBR_EVTUNSUPPFORMAT */ L"error.unsupported.format",
/* VXML_NBR_EVTUNSUPPOBJ */ L"error.unsupported.objectname",
/* VXML_NBR_EVTUNSUPPLANG */ L"error.unsupported.language",
/* VXML_NBR_EVTUNSUPPBUILTIN */ L"error.unsupported.builtin",
/* VXML_NBR_POSTS */ NOENTRY,
/* VXML_NBR_GETS */ NOENTRY,

/*  Table 18 -  Input Counters */

/* VXML_NBR_SPEECH_INPUTS */ NOENTRY,
/* VXML_NBR_DTMF_INPUTS */ NOENTRY,
/* VXML_NBR_FAILED_INPUTS  */ NOENTRY,
/* VXML_NBR_EVTNOINPUTS */ L"noinput",
/* VXML_NBR_EVTNOMATCHES */ L"nomatch",
/* VXML_NBR_EVTMAXSPEECHTIMEOUTS */ L"maxspeechtimeout",
/* VXML_NBR_USERCANCELREQ */ L"cancel",
/* VXML_NBR_USEREXITREQ */ L"exit",
/* VXML_NBR_USRHELPREQ  */ L"help",
/* VXML_NBR_EVTBADGRAMMAR */ L"error.grammar",
/* VXML_NBR_EVTBADINLINEGRAMMAR */ L"error.grammar.inlined",
/* VXML_NBR_EVTBADCHOICEGRAMMAR */ L"error.grammar.choice",
/* VXML_NBR_EVTERRORRECOG */ L"error.recognition",
/* VXML_NBR_EVTERRORRECORD */ L"error.record",
/* VXML_NBR_RECORDINGS */ NOENTRY,

/*  Table 19 -  Output Counters */

/* VXML_NBR_PROMPTS */ NOENTRY,
/* VXML_NBR_TTSSPEAKS */ NOENTRY,
/* VXML_NBR_PRERECSPEAKS */ NOENTRY,
/* VXML_NBR_FAILED_PROMPTS */ NOENTRY,
/* VXML_NBR_ALTTEXT */ NOENTRY,
/* VXML_NBR_REPROMPTS */ NOENTRY,
   
   NULL /*  always last */
};


// !!!! Copy from the header file and quote the ENUM members !!!!
char* VxmlCounterString[] = 
{
// Table 15 -   General Counters

/* VXML_NBR_CALLS */ "NbrCalls",
/* VXML_NBR_EVTNORESOURCE */  "NbrEvtNoResource",
/* VXML_NBR_EVTERRNOAUTH */ "NbrEvtErrNoAuth",
/* VXML_NBR_EVTERROROBJECT */ "NbrEvtErrorObject",
/* VXML_NBR_EVTERRORTRANSFER */ "NbrEvtErrorTransfer",
/* VXML_NBR_UNSUPPORTENCODING */ "NbrUnsupportEncoding",
/* VXML_NBR_DIALOGS */ "NbrDialogs",

/*  Table 16 -  Transfer Counters */
/* VXML_NBR_TRANSFERS */ "NbrTransfers",
/* VXML_NBR_BLINDTRANSFERS */ "NbrBlindTransfers",
/* VXML_NBR_BRIDGEDTRANSFERS */ "NbrBridgedTransfers",
/* VXML_NBR_CONSULTTRANSFERS */ "NbrConsultTransfers",
/* VXML_NBR_TRANSFERFAILS */ "NbrTransferFails",
/* VXML_NBR_XFERFAILCALLEEBUSY */ "NbrXferFailCalleeBusy",
/* VXML_NBR_XFERFAILNETBUSY */ "NbrXferFailNetBusy",
/* VXML_NBR_XFERFAILNOANSWER */ "NbrXferFailNoAnswer",
/* VXML_NBR_EVTCONNNOAUTH */ "NbrEvtConnNoAuth",
/* VXML_NBR_EVTCONNBADDEST */ "NbrEvtConnBadDest",
/* VXML_NBR_EVTCONNNOROUTE */ "NbrEvtConnNoRoute",
/* VXML_NBR_EVTCONNNORSRC */ "NbrEvtConnNoRsrc",
/* VXML_NBR_EVTUNSUPPORTBLIND */ "NbrEvtUnsupportBlind",
/* VXML_NBR_EVTUNSUPPORTBRIDGE */ "NbrEvtUnsupportBridge",
/* VXML_NBR_EVTUNSUPPORTURI */ "NbrEvtUnsupportURI",

/*  Table 17 -  Fetching/Parsing Counters */

/* VXML_NBR_FETCH_ATTEMPTS */ "NbrFetchAttempts",
/* VXML_NBR_EVTBADFETCHES */ "NbrEvtBadFetches",
/* VXML_NBR_EVTBADFETCH_URI */ "NbrEvtBadFetchURI",
/* VXML_NBR_EVTBADFETCH_APPURI */ "NbrEvtBadFetchAppURI",
/* VXML_NBR_EVTBADFETCH_DIALOG */ "NbrEvtBadFetchDialog",
/* VXML_NBR_EVTBADFETCHHTTPERR */ "NbrEvtBadFetchHttpErr",
/* VXML_NBR_DISKCACHEHITS */ "NbrDiskCacheHits",
/* VXML_NBR_DISKCACHEMISS */ "NbrDiskCacheMiss",
/* VXML_NBR_MEMORYCACHEHITS */ "NbrMemoryCacheHits",
/* VXML_NBR_MEMORYCACHEMISS */ "NbrMemoryCacheMiss",
/* VXML_NBR_EVTSEMATICERRORS */ "NbrEvtSematicErrors",
/* VXML_NBR_EVTSEMERR_ECMA */ "NbrEvtSemerrECMA",
/* VXML_NBR_EVTSEMERR_BADTHROW */ "NbrEvtSemErrBadThrow",
/* VXML_NBR_EVTSEMERR_NOGRAM */ "NbrEvtSemErrNoGram",
/* VXML_NBR_EVTUNSUPPFORMAT */ "NbrEvtUnsuppFormat",
/* VXML_NBR_EVTUNSUPPOBJ */ "NbrEvtUnsuppObj",
/* VXML_NBR_EVTUNSUPPLANG */ "NbrEvtUnsuppLang",
/* VXML_NBR_EVTUNSUPPBUILTIN */ "NbrEvtUnsuppBuiltin",
/* VXML_NBR_POSTS */ "NbrPosts",
/* VXML_NBR_GETS */ "NbrGets",

/*  Table 18 -  Input Counters */

/* VXML_NBR_SPEECH_INPUTS */ "NbrSpeechInputs",
/* VXML_NBR_DTMF_INPUTS */ "NbrDTMFInputs",
/* VXML_NBR_FAILED_INPUTS  */ "NbrFailedInputs",
/* VXML_NBR_EVTNOINPUTS */ "NbrEvtNoInputs",
/* VXML_NBR_EVTNOMATCHES */ "NbrEvtNoMatches",
/* VXML_NBR_EVTMAXSPEECHTIMEOUTS */ "NbrEvtMaxSpeechTimeouts",
/* VXML_NBR_USERCANCELREQ */ "NbrUserCancelReq",
/* VXML_NBR_USEREXITREQ */ "NbrUserExitReq",
/* VXML_NBR_USRHELPREQ  */ "NbrUsrHelpReq",
/* VXML_NBR_EVTBADGRAMMAR */ "NbrEvtBadGrammar",
/* VXML_NBR_EVTBADINLINEGRAMMAR */ "NbrEvtBadInlineGrammar",
/* VXML_NBR_EVTBADCHOICEGRAMMAR */ "NbrEvtBadChoiceGrammar",
/* VXML_NBR_EVTERRORRECOG */ "NbrEvtErrorRecog",
/* VXML_NBR_EVTERRORRECORD */ "NbrEvtErrorRecord",
/* VXML_NBR_RECORDINGS */ "NbrRecordings",

/*  Table 19 -  Output Counters */

/* VXML_NBR_PROMPTS */ "NbrPrompts",
/* VXML_NBR_TTSSPEAKS */ "NbrTTSSpeaks",
/* VXML_NBR_PRERECSPEAKS */ "NbrPrerecSpeaks",
/* VXML_NBR_FAILED_PROMPTS */ "NbrFailedPrompts",
/* VXML_NBR_ALTTEXT */ "NbrALTText",
/* VXML_NBR_REPROMPTS */ "NbrReprompts",
   
   
   NULL /*  always last */
};   

char* VxmlGaugeString[] = 
{
 /*  Table 20 -   Gauges */

/* VXML_NBR_LOADEDGRAMMARS */ "NbrLoadedGrammars",
/* VXML_NBR_ACTIVEGRAMMARS */ "NbrActiveGrammars",
/* VXML_NBR_TTSRESOURCES */ "NbrTTSResources",
/* VXML_NBR_LVRRESOURCES */ "NbrLVRResources",

   NULL /*  always last */
};

// Flags for the counters
unsigned int VxmlCounterFlags[] = 
{
// Table 15 -   General Counters

/* VXML_NBR_CALLS */ VXML_NOFLAG,
/* VXML_NBR_EVTNORESOURCE */  VXML_IS_SDR,
/* VXML_NBR_EVTERRNOAUTH */ VXML_IS_SDR,
/* VXML_NBR_EVTERROROBJECT */ VXML_IS_SDR,
/* VXML_NBR_EVTERRORTRANSFER */ VXML_IS_SDR,
/* VXML_NBR_UNSUPPORTENCODING */ VXML_IS_SDR,
/* VXML_NBR_DIALOGS */ VXML_IS_SDR,

/*  Table 16 -  Transfer Counters */
/* VXML_NBR_TRANSFERS */ VXML_NOFLAG,
/* VXML_NBR_BLINDTRANSFERS */ VXML_IS_SDR,
/* VXML_NBR_BRIDGEDTRANSFERS */ VXML_IS_SDR,
/* VXML_NBR_CONSULTTRANSFERS */ VXML_IS_SDR,
/* VXML_NBR_TRANSFERFAILS */ VXML_IS_SDR,
/* VXML_NBR_XFERFAILCALLEEBUSY */ VXML_IS_SDR,
/* VXML_NBR_XFERFAILNETBUSY */ VXML_IS_SDR,
/* VXML_NBR_XFERFAILNOANSWER */ VXML_IS_SDR,
/* VXML_NBR_EVTCONNNOAUTH */ VXML_IS_SDR,
/* VXML_NBR_EVTCONNBADDEST */ VXML_IS_SDR,
/* VXML_NBR_EVTCONNNOROUTE */ VXML_IS_SDR,
/* VXML_NBR_EVTCONNNORSRC */ VXML_IS_SDR,
/* VXML_NBR_EVTUNSUPPORTBLIND */ VXML_IS_SDR,
/* VXML_NBR_EVTUNSUPPORTBRIDGE */ VXML_IS_SDR,
/* VXML_NBR_EVTUNSUPPORTURI */ VXML_IS_SDR,

/*  Table 17 -  Fetching/Parsing Counters */

/* VXML_NBR_FETCH_ATTEMPTS */ VXML_IS_SDR,
/* VXML_NBR_EVTBADFETCHES */ VXML_IS_SDR,
/* VXML_NBR_EVTBADFETCH_URI */ VXML_IS_SDR,
/* VXML_NBR_EVTBADFETCH_APPURI */ VXML_IS_SDR,
/* VXML_NBR_EVTBADFETCH_DIALOG */ VXML_IS_SDR,
/* VXML_NBR_EVTBADFETCHHTTPERR */ VXML_IS_SDR,
/* VXML_NBR_DISKCACHEHITS */ VXML_NOFLAG,
/* VXML_NBR_DISKCACHEMISS */ VXML_NOFLAG,
/* VXML_NBR_MEMORYCACHEHITS */ VXML_NOFLAG,
/* VXML_NBR_MEMORYCACHEMISS */ VXML_NOFLAG,
/* VXML_NBR_EVTSEMATICERRORS */ VXML_IS_SDR,
/* VXML_NBR_EVTSEMERR_ECMA */ VXML_IS_SDR,
/* VXML_NBR_EVTSEMERR_BADTHROW */ VXML_IS_SDR,
/* VXML_NBR_EVTSEMERR_NOGRAM */ VXML_IS_SDR,
/* VXML_NBR_EVTUNSUPPFORMAT */ VXML_IS_SDR,
/* VXML_NBR_EVTUNSUPPOBJ */ VXML_IS_SDR,
/* VXML_NBR_EVTUNSUPPLANG */ VXML_IS_SDR,
/* VXML_NBR_EVTUNSUPPBUILTIN */ VXML_IS_SDR,
/* VXML_NBR_POSTS */ VXML_IS_SDR,
/* VXML_NBR_GETS */ VXML_IS_SDR,

/*  Table 18 -  Input Counters */

/* VXML_NBR_SPEECH_INPUTS */ VXML_IS_SDR,
/* VXML_NBR_DTMF_INPUTS */ VXML_IS_SDR,
/* VXML_NBR_FAILED_INPUTS  */ VXML_IS_SDR,
/* VXML_NBR_EVTNOINPUTS */ VXML_IS_SDR,
/* VXML_NBR_EVTNOMATCHES */ VXML_IS_SDR,
/* VXML_NBR_EVTMAXSPEECHTIMEOUTS */ VXML_IS_SDR,
/* VXML_NBR_USERCANCELREQ */ VXML_IS_SDR,
/* VXML_NBR_USEREXITREQ */ VXML_IS_SDR,
/* VXML_NBR_USRHELPREQ  */ VXML_IS_SDR,
/* VXML_NBR_EVTBADGRAMMAR */ VXML_IS_SDR,
/* VXML_NBR_EVTBADINLINEGRAMMAR */ VXML_IS_SDR,
/* VXML_NBR_EVTBADCHOICEGRAMMAR */ VXML_IS_SDR,
/* VXML_NBR_EVTERRORRECOG */ VXML_IS_SDR,
/* VXML_NBR_EVTERRORRECORD */ VXML_IS_SDR,
/* VXML_NBR_RECORDINGS */ VXML_IS_SDR,

/*  Table 19 -  Output Counters */

/* VXML_NBR_PROMPTS */ VXML_IS_SDR,
/* VXML_NBR_TTSSPEAKS */ VXML_IS_SDR,
/* VXML_NBR_PRERECSPEAKS */ VXML_IS_SDR,
/* VXML_NBR_FAILED_PROMPTS */ VXML_IS_SDR,
/* VXML_NBR_ALTTEXT */ VXML_IS_SDR,
/* VXML_NBR_REPROMPTS */ VXML_IS_SDR,
   
};


NWOmCounter* VxmlCounter[VXML_COUNTER_NULL];
NWOmGauge* VxmlGauge[VXML_GAUGE_NULL];

void MasAdapt::createMeasures() {
   
    // Verify table alignment
   assert( (sizeof(VxmlVxiTransString) / sizeof(VXIchar* )) == (VXML_COUNTER_NULL + 1 )); 
   assert( (sizeof(VxmlCounterString) / sizeof(char* )) == (VXML_COUNTER_NULL + 1 ));
   assert( (sizeof(VxmlGaugeString) / sizeof(char* )) == (VXML_GAUGE_NULL + 1 ));
   assert( (sizeof(VxmlCounterFlags) / sizeof(unsigned int)) == VXML_COUNTER_NULL);  

    // Create counters.
   int i;
         
   for(i=0; VxmlCounterString[i] != NULL; i++) 
   {
      // No longer need to create these (they are in the data fill) 
      //NWOamManager::gNWOamManager->createOMCounter(VxmlCounterString[i]);
      VxmlCounter[i] = NWOamManager::gNWOamManager->getOMCounter(VxmlCounterString[i]);
      if ( wcsncmp(VxmlVxiTransString[i], NOENTRY, sizeof(NOENTRY) ) != 0 ) { // if we have an entry for it
        VXIMapSetProperty(MasAdapt::m_VxiEventMap, VxmlVxiTransString[i],(VXIValue *)VXIIntegerCreate( i ));
      } 
   }
   // Create gauges.
   for(i=0; VxmlGaugeString[i] != NULL; i++) 
   {
      // No longer need to create these (they are in the data fill)
      //NWOamManager::gNWOamManager->createOMGauge(VxmlGaugeString[i]);
      VxmlGauge[i] = NWOamManager::gNWOamManager->getOMGauge(VxmlGaugeString[i]);
   }  
       
} 

