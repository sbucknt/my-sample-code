// VXMLI Counters and gauges 

#ifndef __VXML_MEASURES_H__
#define __VXML_MEASURES_H__

#include <MasAdapt.h>

// defines for counter flags
#define VXML_NOFLAG           0
#define VXML_IS_SDR           1

enum VxmlCounterName 
{
// Table 15 -   General Counters

 VXML_NBR_CALLS, // "Total number of calls received by the VoiceXML interpreter.",
 VXML_NBR_EVTNORESOURCE, // "Total number of error.noresource events thrown.",
 VXML_NBR_EVTERRNOAUTH, // "Total number of error.noauthorization events thrown",
 VXML_NBR_EVTERROROBJECT, // "Total number of error.object events thrown.",
 VXML_NBR_EVTERRORTRANSFER, // "Total number of error.transfer events thrown.",
 VXML_NBR_UNSUPPORTENCODING, // "Total number unsupported encodings requested",
 VXML_NBR_DIALOGS, // "Total number of dialogs.  There are two types of dialogs: forms and menus. Forms present information and gather input; menus offer choices of what to do next",

 // Table 16 -  Transfer Counters 
 
 VXML_NBR_TRANSFERS, // "Total number of transfer attempts.",
 VXML_NBR_BLINDTRANSFERS, // "Total number of blind transfer attempts",
 VXML_NBR_BRIDGEDTRANSFERS, // "Total number of bridge transfer attempts",
 VXML_NBR_CONSULTTRANSFERS, // "Total number of consultative transfer attempts",
 VXML_NBR_TRANSFERFAILS, // "Total number of transfer failures",
 VXML_NBR_XFERFAILCALLEEBUSY, // "Total number of transfer failures due to the callee being busy",
 VXML_NBR_XFERFAILNETBUSY, // "Total number of transfer failures due to the network being busy",
 VXML_NBR_XFERFAILNOANSWER, // "Total number of transfer failures due to the callee not answering within the time specified by the connecttimeout attribute.",
 VXML_NBR_EVTCONNNOAUTH, // "Total number of error.connection.noauthorization events thrown.",
 VXML_NBR_EVTCONNBADDEST, // "Total number of error.connection.baddestination events thrown.",
 VXML_NBR_EVTCONNNOROUTE, // "Total number of error.connection.noroute events thrown.",
 VXML_NBR_EVTCONNNORSRC, // "Total number of error.connection.noresource events thrown.",
 VXML_NBR_EVTUNSUPPORTBLIND, // "Total number of error.unsupported.transfer.blind events thrown.",
 VXML_NBR_EVTUNSUPPORTBRIDGE, // "Total number of error.unsupported.transfer.bridge events thrown",
 VXML_NBR_EVTUNSUPPORTURI, // "Total number of error.unsupported.uri events thrown.",

 // Table 17 -  Fetching/Parsing Counters 

 VXML_NBR_FETCH_ATTEMPTS, // "Total number of attempts to fetch a VoiceXML document",
 VXML_NBR_EVTBADFETCHES, // "Total number of error.badfetch events thrown",
 VXML_NBR_EVTBADFETCH_URI, // "Total number of error.badfetch.bad events thrown.",
 VXML_NBR_EVTBADFETCH_APPURI, // "Total number of error.badfetch.applicationuri  events thrown.",
 VXML_NBR_EVTBADFETCH_DIALOG, // "Total number of error.badfetch.baddialog events thrown.",
 VXML_NBR_EVTBADFETCHHTTPERR, // "Total number of error.badfetch.http.# events thrown.",
 VXML_NBR_DISKCACHEHITS, // "Total number of disk cache hits",
 VXML_NBR_DISKCACHEMISS, // "Total number of disk cache misses",
 VXML_NBR_MEMORYCACHEHITS, // "Total number of memory cache hits",
 VXML_NBR_MEMORYCACHEMISS, // "Total number of memory cache misses",
 VXML_NBR_EVTSEMATICERRORS, // "Total number of error.sematic events thrown.  This event will be thrown when a runtime error is encountered in the VoiceXML document.",
 VXML_NBR_EVTSEMERR_ECMA, // "Total number of error.semantic.ecmascript events thrown.",
 VXML_NBR_EVTSEMERR_BADTHROW, // "Total number of error.semantic.no_event_in_throw events thrown.",
 VXML_NBR_EVTSEMERR_NOGRAM, // "Total number of error.semantic.nogrammars events thrown.",
 VXML_NBR_EVTUNSUPPFORMAT, // "Total number of error.unsupported.format events thrown",
 VXML_NBR_EVTUNSUPPOBJ, // "Total number of error.unsupported.objectname events thrown.",
 VXML_NBR_EVTUNSUPPLANG, // "Total number of error.unsupported.language events thrown",
 VXML_NBR_EVTUNSUPPBUILTIN, // "Total number of error.unsupported.builtin events thrown.",
 VXML_NBR_POSTS, // "Total requests with method set to post",
 VXML_NBR_GETS, // "Total requests with method set to get or undefined, default is a get method",

 // Table 18 -  Input Counters 

 VXML_NBR_SPEECH_INPUTS, // "Total number of speech inputs",
 VXML_NBR_DTMF_INPUTS, // "Total number of DTMF inputs",
 VXML_NBR_FAILED_INPUTS,  // "Total number of failed inputs",
 VXML_NBR_EVTNOINPUTS, // "Total number of noinput events thrown",
 VXML_NBR_EVTNOMATCHES, // "Total number of nomatch events thrown",
 VXML_NBR_EVTMAXSPEECHTIMEOUTS, // "Total number of maxspeechtimeout events thrown",
 VXML_NBR_USERCANCELREQ,  // "The number of pre-defined cancel events thrown.  This occurs when the user speaks �cancel� and the universal cancel grammar is active.",
 VXML_NBR_USEREXITREQ, // "The number of pre-defined exit events thrown.  This occurs when the user speaks �exit� and the universal exit grammar is active.",
 VXML_NBR_USRHELPREQ, // "The number of pre-defined help events thrown.  This occurs when the user speaks �help� and the universal help grammar is active.",
 VXML_NBR_EVTBADGRAMMAR, // "Total number of error.badgrammar events thrown.",
 VXML_NBR_EVTBADINLINEGRAMMAR, // "Total number of error.grammar.inlined events thrown.",
 VXML_NBR_EVTBADCHOICEGRAMMAR, // "Total number of error.grammar.choice events thrown.",
 VXML_NBR_EVTERRORRECOG, // "Total number of error.recognition events thrown.",
 VXML_NBR_EVTERRORRECORD, // "Total number of error.record events thrown",
 VXML_NBR_RECORDINGS, // "Total number of recordings requested",

 // Table 19 -  Output Counters 

 VXML_NBR_PROMPTS, // "Total number of times an attempt was made to speak to the caller.",
 VXML_NBR_TTSSPEAKS, // "Total number of TTS prompts spoken",
 VXML_NBR_PRERECSPEAKS, // "Total number of prerecorded prompts spoken.",
 VXML_NBR_FAILED_PROMPTS, // "Total number of times an attempt to speak to the caller failed.",
 VXML_NBR_ALTTEXT, // "Total number of times the interpreter had to default to speaking alternate text.",
 VXML_NBR_REPROMPTS, // "Total number of time the caller had to be re-prompted.",
   
   VXML_COUNTER_NULL   // always last 
};   

enum VxmlGaugeName 
{
 // Table 20 -  Gauges 

 VXML_NBR_LOADEDGRAMMARS, //  "Number of grammars that are currently loaded",
 VXML_NBR_ACTIVEGRAMMARS, //  "Number of grammars that are currently active",
 VXML_NBR_TTSRESOURCES, //  "Number of TTS resources currently allocated",
 VXML_NBR_LVRRESOURCES, //  "Number of LVR resources currently allocated",

   VXML_GAUGE_NULL  // always last 
};

extern NWOmCounter* VxmlCounter[VXML_COUNTER_NULL];
extern NWOmGauge* VxmlGauge[VXML_GAUGE_NULL];
extern unsigned int VxmlCounterFlags[]; 
extern char* VxmlCounterString[];

#endif // ifndef __VXML_MEASURES_H__