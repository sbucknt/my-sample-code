
#ifndef __VXML_MASADAPT_H__
#define __VXML_MASADAPT_H__

//****************************************************************************
// INCLUDES
//****************************************************************************

// Platform and Session Engine includes
#include <NWTimers.h>
#include <NWPriority.h>
#include <NWOamManager.h>
#include <NWMgmtFW.h>
#include <NWSysManager.h>
#include <NWDBManager.h>
#include <NWIniConfig.h>
#include <NWDlog.h>
#include <CPETraceManager.h>
#include <NWSDRRecordCutter.h>
#include <VXIvalue.h>

#include <VxmlMeasures.h>
#include <string>

#include <../include/OSBdebug/OSBDebugImpl.hpp> // Our implementation of VXIdebug

//****************************************************************************
// LOCAL DEFINES
//****************************************************************************

#define DB_CKEY_NORTELDEFAULTS_FILE "VXML_APPLICATION_DEFAULTS"
#define DB_CKEY_CLIENT_CACHE_CACHEDIR "client.cache.cacheDir"
#define DB_CKEY_CLIENT_LOG_ERRORMAPFILE "client.log.errorMapFile"
#define DB_CKEY_DIAG_TAG "client.log.diagTag."
#define DB_CKEY_DEBUGGER_ENABLE "client.nortel.ivr.debug.debugging"
#define DB_CKEY_VXMLSCEPORT "VXMLSCEPort"
#define DB_CKEY_VXML_EXCEPTION "VXML_EXCEPTION"
#define DB_CKEY_VXML_TRACE "VXMLTrace"
#define DB_CKEY_VXML_DISC_FRM "VXMLDisconnectForm"
#define DB_CKEY_VXML_DISC_FLD "VXMLDisconnectField"
#define DB_CKEY_APPEND_VXML "VXML_STRIP_APPEND_VXML"
#define DB_VALUE_ENABLE "1"
#define DB_MAX_VALUE_SIZE 1024
#define COMPONENT_CFG_ONLY true // for understandability

//****************************************************************************
// STATIC VARIABLES AND FUNCTION PROTOTYPES
//****************************************************************************
#define NW_OAM_VXMLI "VXMLI"
#define NW_OAM_VXMLI_PLAT_INT "Plat"
#define NW_OAM_VXMLI_INTERP_INT "Integ"
#define NW_OAM_VXMLI_SB_LOG "Interp"
#define NW_OAM_VXMLI_LOG_TAG "App"


#ifdef MASADAPT_DLL
#define DECLSPEC __declspec(dllexport)
#else
#define DECLSPEC __declspec(dllimport)
#endif 

/* resolved at runtime (in MasAdapt) to PlatformBridgeImp */
class PlatformBridge {
public:
    virtual VXIdebugResult OSBdebugVshControlImpl(bool enableDebugger, 
        int reportBufferSize,  char *reportBuffer)=0;
};        

/* Instantiated in the main thread (main.cpp and passed into MasAdapt */
class PlatformBridgeImp : public PlatformBridge {
public:
    VXIdebugResult OSBdebugVshControlImpl(bool enableDebugger, int reportBufferSize,  
        char *reportBuffer) {
            return OSBdebugVshControl(enableDebugger, reportBufferSize,  
                  reportBuffer);
    }  
};      

class DECLSPEC MasAdapt : protected NWIniConfigCallback {

private: MasAdapt(PlatformBridge *platform);
  
public: virtual ~MasAdapt();

  // create the instance, init NWCore, counters, trace etc...
public: static void initialize(PlatformBridge *platform); 

  // returns instance of this singleton 
public: static MasAdapt *getInstance( void ); 

  // kick-off VXIMap population via the DB
public: bool setConfigParams(VXIMap **cfg_map);

  // get value of client.log.diagTag.####
public: static bool isDiagTagSet(VXIunsigned tagID);

  // generic config option query
public: static bool isConfigOptionSet(char *dbCkey,bool defaultValue = false); 
  
  // get NortelDefaults.xml (if it changed)
public: VXIbyte *getNortelDefaults(VXIint32 *bufSize);
  
  // to process triggers at runtime
public: void processRuntimeConfig(char *key,char *value,NWIniConfigType type);  
  
  // invoked by NWIniConfig for each parameter
public: void configurationChanged(char *key,char *value,NWIniConfigType type);

  // Append the system working path ass needed
public: bool checkAdjustPath(char *key,char *value,int *valueLen);

public: CPTrace *getInstGlobalDlog( void );

  // returns instance of the platform integration CPTrace object

public: CPTrace *getInstPlatIntLog( void ); 

  // returns instance of the interpreter integration CPTrace object
public: CPTrace *getInstInterpIntLog( void );

  // returns instance of speech browser log CPTrace object
public: CPTrace *getInstSBLog( void );
  
  // returns instance of CPTrace Appliation log trace object 
public: CPTrace *MasAdapt::getInstLogTag( void ) ;  

  // platform integration trace object
private: CPTrace *m_platIntLog;

  // interpreter integration trace object
private: CPTrace *m_interpIntLog; 

  // speech browser Appliation log trace object
private: CPTrace *m_sbLog;
  
  // speech browser log trace object
private: CPTrace *m_logTag; 

  // Temp copy of the VXIMap
private: VXIMap *m_tmpMap;
  
private: PlatformBridge *m_platform;  
  
  // if startup is not complete, then services like trace etc... are not available
public: static bool isAvailable( void ) { return( m_StartupConfigComplete ); };
      
  // indicate startup init complete 
  // (config changed is handled differently after initialization) 
public: static bool m_StartupConfigComplete; 

  // Single instance of this class 
private: static MasAdapt *MasAdapt::m_MasAdapt;

  // Map for translating VXI event names to their counter index
private: static VXIMap *m_VxiEventMap;

public: static void createMeasures();

private: inline static void incSessSDR(int index,char *gslidStr,char *userStr = NULL) 
{
   if (gslidStr == NULL || (VxmlCounterFlags[index] & VXML_NOFLAG) ) {
      return;
   }      
   if (userStr == NULL) {
      NWSDRRecordCutter::gNWSDRRecordCutter->incrementOmInGslidContext(
            gslidStr,VxmlCounterString[index]); 
   }  
   else {
      NWSDRRecordCutter::gNWSDRRecordCutter->writeRecordToGslidContext(
            gslidStr,VxmlCounterString[index],userStr); 
   }  
}   

public: inline static void incOM(VxmlCounterName omname,char *gslidStr = NULL,char *userStr = NULL, int count = 1)
{
   if ( MasAdapt::m_MasAdapt == NULL ) { // one of the utils (like validatedoc) is the user.
      return ;
   }
   if (omname >= 0 && omname < VXML_COUNTER_NULL && VxmlCounter[omname] != NULL ) {
      VxmlCounter[omname]->increment(count);
      incSessSDR(omname,gslidStr,userStr);
   }
}
  // this version of incOM() is used to increment counters from 
  // VXI (they are represented as VXIchar)
  // we sould change VXI code to use an enum and a string table (later)
public: inline static void incOM(const VXIchar* eventName,char *gslidStr = NULL,char *userStr = NULL, int count = 1)
{
  if ( MasAdapt::m_MasAdapt == NULL ) { // one of the utils (like validatedoc) is the user.
      return;
  }

  int i = omIndexFromVxiStr(eventName);

  if ( i >= 0 && i < VXML_COUNTER_NULL && VxmlCounter[i] != NULL ) {
    VxmlCounter[i]->increment(count);
    incSessSDR(i,gslidStr,userStr);
  }   
}

private: inline static int omIndexFromVxiStr(const VXIchar* eventName) 
{
    
  if (m_VxiEventMap == NULL || eventName == NULL) {
    return (-1);
  }  
  if (MasAdapt::getInstance()->getInstPlatIntLog()->m_infoEnabled) {
    char buf[256];
    wcstombs(buf, eventName,255); 
    MasAdapt::getInstance()->getInstPlatIntLog()->info(CPTraceGslid::gNullGslid,"omIndexFromVxiStr() - The event string is %s\n",buf);
  } 
  const VXIValue *val = VXIMapGetProperty(MasAdapt::m_VxiEventMap,eventName); 
  if (VXIValueGetType(val) == VALUE_INTEGER) {
    int retval = (int) VXIIntegerValue((const VXIInteger*) val );
    if (retval >= 0 && retval < VXML_COUNTER_NULL) { // range check
      return (retval);
    }
  }
  return (-1);
  
}  
// Used for trace SDR only (takes a char)
public: inline static void writeSDR(const char* dataName,char *gslidStr = NULL,const char* data = NULL,unsigned long dataLen = 0)
{
  
  if ( (MasAdapt::m_MasAdapt == NULL) || (gslidStr == NULL) ) { // one of the utils (like validatedoc) is the user.
      return;
  }

  // Check for trace flag.
  if (NWIniConfig::gIniConfig->getBoolValByKey(CONFIG_KEY_ENABLE_SYS_DIAG_MODE,CONFIG_KEY_ENABLE_SYS_DIAG_MODE_DEFAULT) == false) {
      return;
  }          

  std::string userStr;
    
  if (dataName != NULL)  {
      userStr.append(dataName);
      userStr.append(" : ");
  }
  if (data != NULL) {
      if (dataLen > 0 ) { 
        userStr.append(data, dataLen); 
      }
      else {
        userStr.append(data);
      }  
  }

  if (!userStr.empty()) {  
    NWSDRRecordCutter::gNWSDRRecordCutter->writeRecordToGslidContext(
            gslidStr,DB_CKEY_VXML_TRACE,userStr.c_str() );
  }
   
};

/* 
 * writeSDR(see arg list below)
 * Wrapper function for writting SDR's to the database. (all args are variable, gslidStr is required)
 *
 * const VXIchar* eventName = NULL  - Name for event
 * char *gslidStr = NULL            - gslid string 
 * const VXIchar* eventData = NULL  - Data for the event 
 * const VXIchar* dialogName = NULL - Form name (or menu)
 * const VXIchar* fieldName = NULL  - Field name
 * const char *DBCkey = NULL        - Option to write to any SDR field name (Default is VXMLTrace)
 *
 */ 
public: inline static void writeSDR(const VXIchar* eventName = NULL,char *gslidStr = NULL,
  const VXIchar* eventData = NULL,const VXIchar* formName = NULL,const VXIchar* fieldName = NULL,const char *DBCkey = NULL)
{
  
  if ( (MasAdapt::m_MasAdapt == NULL) || (gslidStr == NULL) ) { // one of the utils (like validatedoc) is the user.
      return;
  }

  bool isVXMLTrace = false;
  
  if (DBCkey == NULL) {
    isVXMLTrace = true;
  }
    
  /* 
   * We don't want to spend any cycles on VXMLTrace logging if we are not in system diagnostic mode.
   * So, if DBCkey is not set we assume DB_CKEY_VXML_TRACE, then check for system diagnostic mode.
   */
  if (isVXMLTrace && (NWIniConfig::gIniConfig->getBoolValByKey(CONFIG_KEY_ENABLE_SYS_DIAG_MODE,CONFIG_KEY_ENABLE_SYS_DIAG_MODE_DEFAULT) == false) ) {
      return;
  }

  std::string userStr;
  const u_short BUFSIZE = 256;
  char buf[BUFSIZE];
    
  if (eventName != NULL)  {
      if (wcstombs(buf, eventName,(BUFSIZE - 1) ) > 0) { 
      if (isVXMLTrace) {userStr.append("EventName : "); }
      userStr.append(buf,BUFSIZE);
    }  
  }
  if (eventData != NULL) {
    if (wcstombs(buf, eventData,(BUFSIZE - 1) ) > 0) { 
      if (isVXMLTrace) {userStr.append(" EventData : "); }
      userStr.append(buf,BUFSIZE);
    }
  }
  if (formName != NULL) {
    if (wcstombs(buf, formName,(BUFSIZE - 1) ) > 0) { 
      if (isVXMLTrace) {userStr.append(" FormName : "); } 
      userStr.append(buf,BUFSIZE);
    }
  }  
  if (fieldName != NULL) {
    if (wcstombs(buf, fieldName,(BUFSIZE - 1) ) > 0) { 
      if (isVXMLTrace) {userStr.append(" FieldName : "); } 
      userStr.append(buf,BUFSIZE);
    }
  }
  
  if (!userStr.empty()) {  
    if (isVXMLTrace) {
      NWSDRRecordCutter::gNWSDRRecordCutter->writeRecordToGslidContext(
            gslidStr,DB_CKEY_VXML_TRACE,userStr.c_str() );
    }
    else {
      NWSDRRecordCutter::gNWSDRRecordCutter->writeRecordToGslidContext(
            gslidStr,DBCkey,userStr.c_str() );
    }                
  }

}

   
};


#endif // __MASADAPT_H__