#include <MasAdapt.h>
#include <assert.h>

// Resolution of the low resolution timer
#define DEFAULT_LOW_RES_TIMER_RES_MSEC    (1000)

static char gGENSRV_ServiceName[MAX_SERVNAME_SIZE] = NW_OAM_VXMLI;
static char gGENSRV_ServiceNamePlatInt[MAX_SERVNAME_SIZE] = NW_OAM_VXMLI_PLAT_INT;
static char gGENSRV_ServiceNameInterpInt[MAX_SERVNAME_SIZE] = NW_OAM_VXMLI_INTERP_INT;
static char gGENSRV_ServiceNameSBLog[MAX_SERVNAME_SIZE] = NW_OAM_VXMLI_SB_LOG;
static char gGENSRV_ServiceNameLogTag[MAX_SERVNAME_SIZE] = NW_OAM_VXMLI_LOG_TAG;
static char gGENSRV_ServiceVersion[MAX_SERVNAME_SIZE] = "1.00";


// Single instance of this singleton
MasAdapt *MasAdapt::m_MasAdapt = NULL;
VXIMap *MasAdapt::m_VxiEventMap = VXIMapCreate();
bool MasAdapt::m_StartupConfigComplete = false;

MasAdapt::MasAdapt(PlatformBridge *platform)
{
  
  BaseThread::Initialize();
  
  // Allocate a debug object
  CPETraceManager::initializeTrace();
  // NWCore depends on gGlobalDlog being set to a valid trace object  
  gGlobalDlog = m_platIntLog = CPTraceFactory::create(gGENSRV_ServiceNamePlatInt);  
    
  // Create low resolution timer, which must be done before
  // reading the configuration file.
  NWLoResTimer::gNWLoResTimer = xnew NWLoResTimer(DEFAULT_LOW_RES_TIMER_RES_MSEC);
  NWLoResTimer::gNWLoResTimer->start();
  
  // Initialize the DB Manager
  NWDBManager::gNWDBManager = new NWDBManager();
  NWDBManager::gNWDBManager->initialize();

  // Create the global config object
  NWIniConfig::gIniConfig = new NWIniConfig();
  NWIniConfig::gIniConfig->setServiceName(gGENSRV_ServiceName);
  NWIniConfig::gIniConfig->readConfiguration();

  // Configure the debug logging framework
  CPETraceManager::getInstance()->configure();
  
  m_interpIntLog = CPTraceFactory::create(gGENSRV_ServiceNameInterpInt);  
  m_sbLog = CPTraceFactory::create(gGENSRV_ServiceNameSBLog);
  m_logTag = CPTraceFactory::create(gGENSRV_ServiceNameLogTag);
    
  // Initialize the OA&M subsystem
  NWOamManager::gNWOamManager = new NWOamManager(gGENSRV_ServiceName);
  NWOamManager::gNWOamManager->initialize();

  // Initialize OA&M in DB manager, which must be done after 
  // initializing the OA&M subsystem.
  NWDBManager::gNWDBManager->initializeOAM();

  // Initialize NWSDRRecordCutter
  NWSDRRecordCutter::gNWSDRRecordCutter = new NWSDRRecordCutter();
  NWSDRRecordCutter::gNWSDRRecordCutter->enableAsDistributedSessionManager();

  // init counters and gauges
  createMeasures();

  // Set the priority of the process.
  SetProcessPriorityLevel(NWPROCESS_PRIORITY_NORMAL);

  m_tmpMap = VXIMapCreate();
  
  m_platform = platform;
  
}
  
MasAdapt::~MasAdapt()
{
  VXIMapDestroy( &m_tmpMap );
  VXIMapDestroy( &m_VxiEventMap );
}

  
//------------------------------------------------------------------------------------
// bool MasAdapt::initialize()
// 
// NOTE: Here we intialize the MAS interface singleton. 
//
//------------------------------------------------------------------------------------
void MasAdapt::initialize(PlatformBridge *platform)
{
  // Ensure that we are not currently initialized
  assert( MasAdapt::m_MasAdapt == NULL );
  
  // Create a new instance of this singleton
  MasAdapt::m_MasAdapt = new MasAdapt(platform);
  
}
// returns pointer to an instance of this singleton
MasAdapt *MasAdapt::getInstance( void ) 
{ 
  assert( m_MasAdapt != NULL );
  return (m_MasAdapt); 
}
// returns global CPTrace object pointer
CPTrace *MasAdapt::getInstGlobalDlog( void ) 
{ 
  assert( gGlobalDlog != NULL );
  return (gGlobalDlog); 
}

// returns platform integration CPTrace object pointer
CPTrace *MasAdapt::getInstPlatIntLog( void ) 
{ 
  assert( m_platIntLog != NULL );
  return (m_platIntLog); 
}

// returns platform integration CPTrace object pointer
CPTrace *MasAdapt::getInstInterpIntLog( void ) 
{ 
  assert( m_interpIntLog != NULL );
  return (m_interpIntLog); 
}

// returns speech browser CPTrace object pointer
CPTrace *MasAdapt::getInstSBLog( void ) 
{ 
  assert( m_sbLog != NULL );
  return (m_sbLog); 
}

CPTrace *MasAdapt::getInstLogTag( void ) 
{ 
  assert( m_logTag != NULL );
  return (m_logTag); 
}


//------------------------------------------------------------------------------------
// bool MasAdapt::checkAdjustPath(char *key,char *value,int *valueLen)
//
// Note: appends the value with the systems working directory for the following keys:
//  client.log.errorMapFile.xxx (all of these)  
//  client.cache.cacheDir
//------------------------------------------------------------------------------------
bool MasAdapt::checkAdjustPath(char *key,char *value,int *valueLen) 
{
  if ((strcmp(key,DB_CKEY_CLIENT_CACHE_CACHEDIR) == 0) || 
     (strncmp(key,DB_CKEY_CLIENT_LOG_ERRORMAPFILE,sizeof(DB_CKEY_CLIENT_LOG_ERRORMAPFILE) -1) == 0)) {
    NWString *tmpPath = xnew NWString();
    NWIniConfig::gIniConfig->getWorkingDirByKey(key, value,*tmpPath);
    if (!tmpPath->empty() && (tmpPath->length() < DB_MAX_VALUE_SIZE)) {
      *valueLen = (sprintf(value,"%s",tmpPath->get()) + 1);
      delete tmpPath;
      return true;
    }
    delete tmpPath; 
    } 
    return false;
} 

//------------------------------------------------------------------------------------
// void processRuntimeConfig(char *key,char *value,NWIniConfigType type);
//
//  process configorationChanged after startup
//  We don't want to update the map on the fly for  
//  every option that changes after startup
//------------------------------------------------------------------------------------
void MasAdapt::processRuntimeConfig(char *key,char *value,NWIniConfigType type) {
  m_platIntLog->info(CPTraceGslid::gNullGslid,"%s","MasAdapt::processRuntimeConfig() - Recieved runtime config notification.\n");

  if (strcmp(key, DB_CKEY_DEBUGGER_ENABLE) == 0) {
    bool enable = false;
    if (strcmp(value,"1") == 0) {
      enable = true;
    }
     
    char tmpBuf[4096]; // needed to see result string
    if (m_platform->OSBdebugVshControlImpl(enable,4096,tmpBuf) != VXIdebug_RESULT_SUCCESS) {
        m_platIntLog->info(CPTraceGslid::gNullGslid,"MasAdapt::processRuntimeConfig() - OSBdebugVshControlImpl() - Failed.\n");
    }    
    m_platIntLog->info(CPTraceGslid::gNullGslid,"MasAdapt::processRuntimeConfig() - VXIdebug_RESULT_SUCCESS from OSBdebugVshControlImpl()\n");
    m_platIntLog->info(CPTraceGslid::gNullGslid,"%s\n",tmpBuf);

  }
  return;
} 
// Need to quickly return the value of the diagTag
bool MasAdapt::isDiagTagSet(VXIunsigned tagID) {

  if ( MasAdapt::m_MasAdapt == NULL ) { // one of the utils (like validatedoc) is the user.
      if (tagID == 8000) {
        return (true); // Return true so validate doc can display parse error info
      }
      return (false); 
  }
  
  char keyBuf[DB_MAX_VALUE_SIZE];
  sprintf(keyBuf, "%s%d", DB_CKEY_DIAG_TAG, tagID);
  
  if (!NWIniConfig::gIniConfig->getBoolValByKey(keyBuf,false)) {
    return (false);
  } 

  return (true); 
  
} 

//------------------------------------------------------------------------------------
// bool MasAdapt::isConfigOptionSet(const char* dbCkey)
//
// Note: Generic option query. takes an ckey as an argument and returns true
//       if the option is enabled, false if not enable (or non-existent)
//       (optionally you may specify a true defaultValue)
//------------------------------------------------------------------------------------
bool MasAdapt::isConfigOptionSet(char *dbCkey, bool defaultValue) {

  if ( MasAdapt::m_MasAdapt == NULL || dbCkey == NULL) { // one of the utils (like validatedoc) is the user.
      return (false); 
  }
  
  if (NWIniConfig::gIniConfig->getBoolValByKey(dbCkey, defaultValue) ) {
    return (true);  
  }  

  return (false); 
  
} 

//------------------------------------------------------------------------------------
// VXIbyte *MasAdapt::setNortelDefaults(VXIint32 *bufSize)
//
// Returns  : A pointer to a VXIbyte buffer (this is what the vxi code requires)
//    or NULL if the query fails.
//
// NOTE: The VXI code will free the buffer.
//
//------------------------------------------------------------------------------------
VXIbyte *MasAdapt::getNortelDefaults(VXIint32 *bufSize)
{

  char *sval = NULL;
  NWString nwSval;

  // Check to make sure above is done correctly (make sure nodeID and CompID are not 0)
  if (!NWIniConfig::gIniConfig->isConfigurationValid()) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"Error : NWIniConfig::isConfigurationValid()\n");
    return (NULL);
  } 

  // See if the key exists
  
  if (!NWIniConfig::gIniConfig->configurationKeyExists(DB_CKEY_NORTELDEFAULTS_FILE)) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"ckey : %s Not found in the database\n",DB_CKEY_NORTELDEFAULTS_FILE);
    return (NULL);
  }
  
  // Obtain the value
  
  if (!NWIniConfig::gIniConfig->getCharValByKey(DB_CKEY_NORTELDEFAULTS_FILE,nwSval)) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"Not able to obtain %s value from the database\n",DB_CKEY_NORTELDEFAULTS_FILE);
    return (NULL);
  }
    
  // Convert value from NWString to char *, Add it to the vximap
  
  sval = nwSval.get();
  
  if (sval != NULL) { 

    *bufSize =   (int)strlen(sval)+1;
    VXIbyte *result = new VXIbyte[*bufSize];

    memcpy(static_cast<void *>(result), static_cast<void *>(sval),
      *bufSize * sizeof(char));
    m_platIntLog->info(CPTraceGslid::gNullGslid,"setNortelDefaults() : Success!\n");
    return(result);

  } 
  else {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"setNortelDefaults() : Failed!\n");
    return (NULL);
    }
  
}

// Populate the VXImap (m_tmpMap)
void MasAdapt::configurationChanged(char *key,char *value,NWIniConfigType type) {
  // check for NULL for key
  if (key == NULL) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"The key is NULL!!");
    return;
  } 
  // check for NULL for value
  if (value == NULL) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"The value is NULL!!");
    return;
  } 
  
  if ( m_StartupConfigComplete == true ) {
    processRuntimeConfig(key, value, type);
    return;
  } 

  if (strcmp(key, DB_CKEY_NORTELDEFAULTS_FILE) == 0) {
    // we don't want to keep the NotelDefaults.xml in the VXIMap (so skip it).
    return;
  }
   
  if (strcmp(key, DB_CKEY_DEBUGGER_ENABLE) == 0) {
    // register for trigger for dynamic debugger enable  
    NWIniConfig::gIniConfig->registerNotification(key,*this);
  }
  
  if (value != NULL && strlen(value) > DB_MAX_VALUE_SIZE) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"The value is too big to process (greater than %d), Skip it!\n",DB_MAX_VALUE_SIZE);
    return;
  }
  
  if (m_tmpMap == NULL) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"The VXIMap is NULL!!");
    return;
  } 
    
  // FIXME - need a logical way to decide how big the buffers should be???
  char qKey[DB_MAX_VALUE_SIZE],qValue[DB_MAX_VALUE_SIZE];

  VXIint valueLen = 0,keyLen = 0;
  
  keyLen = (sprintf(qKey,"%s",key) + 1);

  if (value != NULL) {
    valueLen = (sprintf(qValue,"%s",value) + 1);
  }
  
  // We may need to adjust a value with the working directory path
  if (type == NWIniConfigTypeString && checkAdjustPath(qKey, qValue, &valueLen)) {
    m_platIntLog->info(CPTraceGslid::gNullGslid,"The path was adjusted for %s, the new value is %s\n",qKey,qValue);
  }
  
  VXIchar *wConfigKey = new VXIchar [keyLen];
  
  // assign the parameter to the wConfigKey
  mbstowcs(wConfigKey, qKey, keyLen);

  if(type == NWIniConfigTypeInt) {
          VXIint32 intValue = (VXIint32)atoi(qValue);
          if((intValue == 0) && (value[0] != '0')) {
                  delete [] wConfigKey;
      m_platIntLog->info(CPTraceGslid::gNullGslid,"VXIplatform_RESULT_FAILURE\n");
      return;
          }
    VXIMapSetProperty(m_tmpMap, wConfigKey, 
                   (VXIValue *)VXIIntegerCreate(intValue));
  }

  if(type == NWIniConfigTypeString) {
          VXIchar *wConfigValue = new VXIchar [valueLen];
          if(wConfigValue == NULL) {
                  delete [] wConfigKey;
                  m_platIntLog->info(CPTraceGslid::gNullGslid,"VXIplatform_RESULT_OUT_OF_MEMORY\n");
      return;
          }
    mbstowcs(wConfigValue, qValue, valueLen);
    VXIMapSetProperty(m_tmpMap, wConfigKey, 
                   (VXIValue *)VXIStringCreate(wConfigValue));
    delete [] wConfigValue;

  }
  delete [] wConfigKey;
  
}   


//------------------------------------------------------------------------------------
// bool MasAdapt::setConfigParams(VXIMap *cfg_map)
//
// Args: a pointer to a VXImap (created prior to the call)
//
// Returns  : true, if the queury for the NortelDefaults.xml (blob) contents succeeds.
//    Otherwise false.
//
// NOTE: The VXI code will free the buffer.
//
//------------------------------------------------------------------------------------
bool MasAdapt::setConfigParams(VXIMap **cfg_map)
{
  NWDBQuery query;
  static char *sval;
  int rows = 0;

  // Assign the cfg_map to MasAdapt VXIMap (m_tmpMap) so we can build it up in this class

  *cfg_map = m_tmpMap; // Now the pointer passed to us points to our newly created VXIMap

  // This will cause configurationChanged() to be called for each key
  NWIniConfig::gIniConfig->synchronizeConfiguration(*this,COMPONENT_CFG_ONLY);

  MasAdapt::m_StartupConfigComplete = true;
  
  m_platIntLog->info(CPTraceGslid::gNullGslid,"setConfigParams() - Complete.\n");
  return true;



}

